/*WAP to take a number as input and print the addition of factorial of each digit from that number
INPUT:1234
OUTPUT:Addition of factorial of each digit from 1234=33
*/
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number");
		int num=Integer.parseInt(br.readLine());
		int temp=num;
		int sum=0;
		while(num!=0){
			int rem=num%10;
			int fact=1;
			for(int i=rem;i>=1;i--){
				fact=fact*i;
			}
			sum=sum+fact;
			num=num/10;
		}
		System.out.println("Addition of factorial of each digit from "+temp+"="+sum);
	}
}

