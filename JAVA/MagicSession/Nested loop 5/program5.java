import java.io.*;
class Pattern{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter row");
		int row=Integer.parseInt(br.readLine());
		int a=0;
		int b=1;
		int c;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print(a+" ");
				c=a+b;
				a=b;
				b=c;
			}
			System.out.println();
		}
	}
}
