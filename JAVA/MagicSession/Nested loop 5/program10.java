//WAP to print a series of prime numbers from entered range(take start and end number)
import java.io.*;
class PrimeSeries{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter starting number");
		int start=Integer.parseInt(br.readLine());
		System.out.println("Enter ending number");
                int end=Integer.parseInt(br.readLine());

		for(int i=start;i<=end;i++){
			int num=i;
			int count=0;
			for(int j=1;j<=num;j++){
				if(num%j==0){
			        	count++;
				}
			}
			if(count==2){
				System.out.print(num+" ");
			}
		}
		System.out.println();
	}
}

