/*
5	4	3	2	1	
8	6	4	2	
9	6	3	
8	4	
5
*/

import java.io.*;
class Pattern{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of rows");
		int row=Integer.parseInt(br.readLine());
                int num=row;
		int temp=num;

		for(int i=1;i<=row;i++){
			for(int j=row;j>=i;j--){
				System.out.print(temp*i+"\t");
				temp--;
			}
			temp=num-i;
			System.out.println();
		}
	}
}

