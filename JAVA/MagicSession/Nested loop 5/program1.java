/*
D4 C3 B2 A1 
A1 B2 C3 D4 
D4 C3 B2 A1 
A1 B2 C3 D4 
*/

import java.io.*;
class Pattern{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of rows");
		int row=Integer.parseInt(br.readLine());
		int num=row;
		char ch=(char)(64+row);
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(i%2==1){
					System.out.print(ch-- + ""+num--+ " ");
				}else{
					System.out.print(ch++ +""+num++ +" ");
				}
			}
				if(num==0){
					ch=65;
					num=1;
				}else{
					ch=(char)(64+row);
					num=row;
				}
			
			System.out.println();
		}
	}
}


