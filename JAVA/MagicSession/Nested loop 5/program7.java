/*
O
14 13
L  K  J
9  8  7  6
E  D  C  B  A
*/
import java.io.*;
class Pattern{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter row:");
		int row=Integer.parseInt(br.readLine());
		int num=(row*(row+1))/2;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(row%2==1){
					char ch1=(char)(64+num);
					if(i%2==1){
						System.out.print(ch1+"\t");
					}else{
						System.out.print(num+"\t");
					}
					ch1--;
					num--;
				}else{
					char ch1=(char)(64+num);
                                        if(i%2==1){
                                                System.out.print(num+"\t");
                                        }else{
                                                System.out.print(ch1+"\t");
                                        }
                                        ch1--;
                                        num--;
				}
			}
			System.out.println();
		}
	}
}
