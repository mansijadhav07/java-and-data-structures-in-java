/*WAP to reverse the number
  INPUT:942111423
  OUTPUT:324111249
*/
class Reversenum{
	public static void main(String[] args){
		int N=942111423;
		int reverse=0;
		while(N!=0){
			int rem=N%10;
			reverse=(reverse*10)+rem;
			N=N/10;
		}
		
		System.out.println("Reverse is " +reverse);
	}
}


