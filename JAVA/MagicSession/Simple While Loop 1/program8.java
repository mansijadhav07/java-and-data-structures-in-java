/* WAP to print count down of days 
   to submit the assignment
   INPUT : day=7
   OUTPUT: 7 days remaining
           6 days remaining
           5 days remaining
	   .
	   .
	   .
	   1 days remaining
	   0 days Assignment is overdue
*/
class Countdown{
	public static void main(String[] args){
		int day=7;
		while(day>=1){
			System.out.println(day+ " days remaining");
			day--;
		}
		if(day==1){
			System.out.println(day+ " day remaining");
			day--;
		}
		
		System.out.println(day+ " days Assignment is overdue");
	}
}

