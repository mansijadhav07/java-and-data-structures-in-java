/*WAP to print the square of the even digit of given number
  INPUT:942111423
  OUTPUT:4 16 4 16
*/
class EvenSquare{
	public static void main(String[] args){
		int num=942111423;
		while(num!=0){
			int rem=num%10;
			if(rem%2==0){
				System.out.println(rem*rem);
			}
			num=num/10;
		}
	}
}
