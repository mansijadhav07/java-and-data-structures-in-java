/*WAP to print the sum of all even numbers and multiplication of odd numbers between 1 to 10
  OUTPUT: SUM=30
          MULTIPLICATION=945
*/
class SumMult{
	public static void main(String[] args){
		int N=10;
		int sum=0;
		int mult=1;
		int i=1;
		while(i<=N){
			if(i%2==0){
				sum=sum+i;
			}else{
				mult=mult*i;
			}
			i++;
		}
		System.out.println("Sum of even numbers between 1 to 10 is " +sum);
		System.out.println("Multiplication of odd numbers between 1 to 10 is "+mult);
	}
}
