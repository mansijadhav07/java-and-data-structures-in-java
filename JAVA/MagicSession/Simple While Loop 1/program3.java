/* WAP to print count of the digits of given number
   INPUT: 942111423
   OUTPUT:Count of digit is 9
*/
class CountDigit{
	public static void main(String[] args){
		int num=942111423;
		int count=0;
		while(num!=0){
			count++;
			num=num/10;
		}
		System.out.println("Count of digit is "+count);
	}
}
