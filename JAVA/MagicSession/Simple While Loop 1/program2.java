/* WAP to print factorial of N
   INPUT:6
   OUTPUT:720
*/
class Factorial{
	public static void main(String[] args){
		int N=6;
		int temp=N;
		int fact=1;
		while(N>=1){
			fact=fact*N;
			N--;
		}
		System.out.println("Factorial of "+temp+ " is " +fact);
	}
}
