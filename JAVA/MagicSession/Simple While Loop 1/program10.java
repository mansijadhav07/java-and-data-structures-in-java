/*WAP to check given number is palindrome
 INPUT:2332
 OUTPUT:2332 is palindrome*/
class Ispalindrome{
	public static void main(String[] args){
		int num=2332;
		int reverse=0;
		int temp=num;

		while(num!=0){
			int rem=num%10;
			reverse=(reverse*10)+rem;
			num=num/10;
		}

		if(reverse==temp){
			System.out.println(temp+" is palindrome number");
		}else{
			System.out.println(temp+" is not palindrome number");
		}
	}
}

  
