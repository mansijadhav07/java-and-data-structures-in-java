/*WAP to print count of odd digit of given number
  INPUT:942111423
  OUTPUT:Count of odd digit is 5
*/
class OddDigit{
	public static void main(String[] args){
		int num=942111423;
		int count=0;
		while(num!=0){
			int rem=num%10;
			if(rem%2==1){
				count++;
			}
			num=num/10;
		}
		System.out.println("Count of odd digit is "+count);
	}
}
