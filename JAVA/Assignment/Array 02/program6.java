//WAP to take size of array from user and also take integer elements from user 
//find the maximum element from an array
import java.io.*;
class MaxElement{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Size of an array");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];
                int max=0;
                System.out.println("Enter array elements");
                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                        max=arr[0];
                        if(max<arr[i]){
                                 max=arr[i];
                        }
                }
                System.out.println("Maximum element of array is "+max);
        }
}
