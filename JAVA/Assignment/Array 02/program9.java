//WAP to merge two arrays by taking third array
import java.io.*;
class Merge{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Size of first array");
                int size1=Integer.parseInt(br.readLine());

                System.out.println("Enter Size of second array");
                int size2=Integer.parseInt(br.readLine());


                int arr1[]=new int[size1];
                int arr2[]=new int[size2];

                System.out.println("Enter first array elements");
                for(int i=0;i<size1;i++){
                        arr1[i]=Integer.parseInt(br.readLine());
                }

                System.out.println("Enter second array elements");
                for(int i=0;i<size2;i++){
                        arr2[i]=Integer.parseInt(br.readLine());
                }
                   

		int size=size1+size2;
		int arr[]=new int[size];
		int j=0,k=0;
		for(int i=0;i<arr.length;i++){
			if(j<arr1.length){
				arr[i]=arr1[j];
				j++;
			}else{
                                arr[i]=arr2[k];
				k++;
                        }
                }
                System.out.println("First array");
                for(int i=0;i<size1;i++){
                        System.out.print("|"+arr1[i]+"|");
                }
                System.out.println();

                System.out.println("Second array");
                for(int i=0;i<size2;i++){
                        System.out.print("|"+arr2[i]+"|");
                }
                System.out.println();
                
		System.out.println("Merged array");
                for(int i=0;i<arr.length;i++){
                        System.out.print("|"+arr[i]+"|");
                }
                System.out.println();
	}
}


