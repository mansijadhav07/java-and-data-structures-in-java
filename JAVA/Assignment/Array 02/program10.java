//WAP to print elements whose addition of digits is even
import java.io.*;
class DigitEvenSum{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Size of array");
                int size=Integer.parseInt(br.readLine());
 
                int arr[]=new int[size];
               

                System.out.println("Enter array elements");
                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                int i=0;
		System.out.println("Sum of digit is even elements in array");
                while(i<arr.length){
		          int num=arr[i];
			  int sum=0;
		          while(num!=0){
			        int rem=num%10;
		                 sum=sum+rem;
		                 num=num/10;
			  }
	                  if(sum%2==0){
		                   System.out.println(arr[i]);
	                  }
                          i++;
		}
	}
}

