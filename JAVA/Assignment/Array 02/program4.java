//WAP to search a specific element from an array and return its index

import java.io.*;
class SearchEle{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Size of an array");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];
                System.out.println("Enter array elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter element to be searched:");
		int element=Integer.parseInt(br.readLine());
		for(int i=0;i<size;i++){
			if(element==arr[i]){
				System.out.println("Element found at index "+i);
			}
                }
        }
}
   
