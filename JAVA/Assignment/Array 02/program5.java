//WAP to take size of array from user and also take integer elements from user find minimum element from array

import java.io.*;
class MinElement{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Size of an array");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];
		int min=0;
                System.out.println("Enter array elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
			min=arr[0];
                        if(min>arr[i]){
			         min=arr[i];
                        }
		}
                System.out.println("Minimum element of array is "+min);

        }
}
     
