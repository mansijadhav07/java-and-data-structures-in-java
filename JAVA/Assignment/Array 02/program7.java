//WAP to find the common elements between two arrays
import java.io.*;
class CommonEle{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Size of first array");
                int size1=Integer.parseInt(br.readLine());

		System.out.println("Enter Size of second array");
                int size2=Integer.parseInt(br.readLine());


                int arr1[]=new int[size1];
		int arr2[]=new int[size2];

                System.out.println("Enter first array elements");
                for(int i=0;i<arr1.length;i++){
                        arr1[i]=Integer.parseInt(br.readLine());
                }

		System.out.println("Enter second array elements");
                for(int i=0;i<arr2.length;i++){
                        arr2[i]=Integer.parseInt(br.readLine());
                }

		System.out.println("First array");
                for(int i=0;i<arr1.length;i++){
                        System.out.print("|"+arr1[i]+"|");
                }
		System.out.println();

		System.out.println("Second array");
                for(int i=0;i<arr2.length;i++){
                        System.out.print("|"+arr2[i]+"|");
                }
		System.out.println();

		System.out.println("Common elements");
                for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr2.length;j++){
		        	if(arr1[i]==arr2[j]){
			        	System.out.println(arr1[i]);
					break;
				}
			}
		}
                
        }
}
