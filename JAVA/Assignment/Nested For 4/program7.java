/*
F	
E	1	
D	2	E	
C	3	D	4	
B	5	C	6	D	
A	7	B	8	C	9
*/
class Pattern{
	public static void main(String[] args){
		int num=1;
		char ch1='F';
		int row=6;
		for(int i=1;i<=row;i++){
			char ch2=ch1;
			for(int j=1;j<=i;j++){
				if(j%2==0){
					System.out.print(num++ +"\t");
			        }else{
					System.out.print(ch2++ +"\t");
				}
			}
		        System.out.println();
			ch1--;
			
		}
	}
}
