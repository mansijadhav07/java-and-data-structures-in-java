/*
10	
I	H	
7	6	5	
D	C	B	A
*/
class Pattern{
	public static void main(String[] args){
		int row=4;
		char ch='J';
		int num=(row*(row+1))/2;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
			        if(i%2==0){
					System.out.print(ch+ "\t");
				}else{
					System.out.print(num+"\t");
				}
				ch--;
				num--;
			}
			System.out.println();
		}
	}
}
