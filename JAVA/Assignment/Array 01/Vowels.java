//Take 7 characters as an input print only vowels from the array
import java.io.*;
class Array{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size");
		int size=Integer.parseInt(br.readLine());

		char carr[]=new char[size];
                System.out.println("Enter array elements");
		for(int i=0;i<carr.length;i++){
			carr[i]=(char)(br.read());
			br.skip(1);
		}
                System.out.println("Vowels in array are");
		for(int i=0;i<carr.length;i++){
			if(carr[i]=='a'|| carr[i]=='e'||carr[i]=='i'|| carr[i]=='o'|| carr[i]=='u'|| carr[i]=='A'|| carr[i]=='E'||carr[i]=='I'|| carr[i]=='O'|| carr[i]=='U'){
				System.out.println(carr[i]);
			}
		}
	}
}

	

