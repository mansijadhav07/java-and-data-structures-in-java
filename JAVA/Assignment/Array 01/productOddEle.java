//WAP to take array from user and also take integer elements from user print product of odd index array
import java.io.*;
class ProductOddIndex{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Size of an array");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];
                int product=1;
                System.out.println("Enter array elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                        if(i%2==1){
                                   product=product*arr[i];
                        }

                }
                System.out.println("Product of odd array elements is "+product);
        }
}

