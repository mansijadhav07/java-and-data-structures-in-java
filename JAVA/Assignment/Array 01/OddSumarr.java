//WAP to take size of array from user and also take integer elements from user print sum of odd elements only
import java.io.*;
class ArrayOddSum{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Size of an array");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];
                int oddSum=0;
                System.out.println("Enter array elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                        if(arr[i]%2!=0){
			         oddSum=oddSum+arr[i];
                        }
		}
                System.out.println("Sum of odd array elements is "+oddSum);

        }
}
     

