//Write a program take 10 input from the user and print only elements that are divisible by 5
import java.io.*;
class Div5{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Size of an array");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];
                
                System.out.println("Enter array elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

		System.out.println("Array elements divisible by 5 are");
		  for(int i=0;i<size;i++){
                        if(arr[i]%5==0){
                                   System.out.println(arr[i]);
                        }
		  }
        }
}

