/* 1  2  9
   4  25 6
   49 8  81*/
class Pattern{
	public static void main(String[] args){
		int N=1;
		for(int i=1;i<=3;i++){
			for(int j=1;j<=3;j++){
				if((i+j)%2==0){
					System.out.print(N*N+"\t");
				}else{
					System.out.print(N+"\t");
				}
				N++;
			}
			System.out.println();
		}
	}
}
