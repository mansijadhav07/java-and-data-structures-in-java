/*Write a program to count the digits of the given number
  INPUT:942111423
  OUTPUT:Count of digits=9*/
class Countdigit{
	public static void main(String[] args){
		int N=942111423;
		int count=0;
		for(int i=N;i>0;i=i/10){
			count++;
		}
		System.out.println("Count of digits="+count);
	}
}


