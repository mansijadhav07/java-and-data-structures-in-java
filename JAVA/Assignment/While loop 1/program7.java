/*Write a program which takes number from user's if number
  is even print that number in reverse order or if number is odd
  print that number in reverse order by difference of two
  INPUT:6
  OUTPUT:6 5 4 3 2 1
  INPUT:7
  OUTPUT: 7 5 3 1
*/
class Reverse{
	public static void main(String[] args){
		int N=7;
		if(N%2==0){
			for(int i=N;i>0;i--){
				System.out.print(i+" ");
			}
			System.out.println();
		}else{
			for(int i=N;i>0;i=i-2){
                                System.out.print(i+" ");
                        }
			System.out.println();
		}
	}
}
