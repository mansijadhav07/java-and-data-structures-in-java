/*Write a program to print the square of even digits of the given number
  INPUT:942111423
  OUTPUT:4 16 4 16*/
class Square{
	public static void main(String[] args){
		int N=942111423;
	
		for(int i=N;i>0;i=i/10){
			int rem=i%10;
			if(rem%2==0){
			          System.out.println(rem*rem);
			}
		}
		
	}
}


