/*Write a program to print the sum of all even numbers and
  Multiplication of odd numbers between 1 to 10
  OUTPUT:Sum of even numbers between 1 to 10=30
         Multiplication of odd numbers between 1 to 10=945
*/
class Sumult{
	public static void main(String[] args){
		int sum=0;
		int mult=1;
		for(int i=1;i<=10;i++){
			if(i%2==0){
				sum=sum+i;
			}else{
				mult=mult*i;
			}
		}
		System.out.println("Sum of even numbers between 1 to 10="+sum);
		System.out.println("Multiplication of odd numbers between 1 to 10="+mult);
	}
}


