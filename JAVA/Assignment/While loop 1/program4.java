/*Write a program to count the odd digits of the given number
  INPUT:942111423
  OUTPUT:Count of odd digits=5*/
class CountOdddigit{
	public static void main(String[] args){
		int N=942111423;
		int count=0;
		for(int i=N;i>0;i=i/10){
			int rem=i%10;
			if(rem%2==1){
			          count++;
			}
		}
		System.out.println("Count of odd digits="+count);
		
	}
}


