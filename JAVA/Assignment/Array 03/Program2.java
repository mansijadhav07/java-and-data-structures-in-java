/*
WAP to reverse each element in an array
Take size and elements from the user
Input: 10 25 252 36 564
Output: 01 52 252 63 465
*/

import java.io.*;
class ReverseEle{
         static void ReverseDigitArr(int arr[]){

                int num;
                for(int i=0;i<arr.length;i++){
                      
                         num=arr[i];
		         int reverse=0;	 
                         while(num!=0){
				int rem=num%10;
				reverse=reverse*10+rem;
                                num=num/10;
                         }
	        	 arr[i]=reverse;
                }

        }

        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int size;
                System.out.println("Enter array size");
                do{
                        size=Integer.parseInt(br.readLine());
                        if(size<=0){
                                System.out.println("Re-Enter array size");
                        }
                }while(size<=0);

                int arr[] =new int[size];

                System.out.println("Enter array elements");
                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                System.out.println("Array");
                for(int i=0;i<arr.length;i++){
                        System.out.print("|"+arr[i]+"|");
                }

                System.out.println();

		ReverseDigitArr(arr);
		System.out.println("Array");
                for(int i=0;i<arr.length;i++){
                        System.out.print("|"+arr[i]+"|");
                }

		System.out.println();
	}
}




