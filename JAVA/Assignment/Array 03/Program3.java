/*
WAP to find composite number from an array and return index
Take size and elements from user
Input: 1 2 3 4 5 6 7
Output:composite 6 found at index : 4
*/
import java.io.*;
class CompositeNum{
	static void Composite(int arr[]){
	        for(int i=0;i<arr.length;i++){
			   int num=arr[i];
                            int count=0;
			    if(num==0|| num==1){
				    count=2;
			    }else{
		                   for(int j=1;j<=num;j++){
			                 if(num % j==0){
			                	count++;
				        }
				   }
				   if(count != 2){
					   System.out.println("Composite "+num+" found at index "+i);
				   }

			  }
		}
	}
			  
	public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int size;
                System.out.println("Enter array size");
                do{
                        size=Integer.parseInt(br.readLine());
                        if(size<=0){
                                System.out.println("Re-Enter array size");
                        }
                }while(size<=0);

                int arr[] =new int[size];

                System.out.println("Enter array elements");
                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                System.out.println("Array");
                for(int i=0;i<arr.length;i++){
                           System.out.print("|"+arr[i]+"|");
		}
		System.out.println();
		Composite(arr);
	}
}


