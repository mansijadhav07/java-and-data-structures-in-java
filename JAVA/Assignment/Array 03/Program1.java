/*
Write a program to print count of digits in elements of array.
Input:Enter array elements:02 255 2 1554
Output: 2 3 1 4
*/
import java.io.*;
class CountDigit{	
	 static void CountDigitArr(int arr[]){
                
		int num;
                for(int i=0;i<arr.length;i++){
			int count=0;
                         num=arr[i];
			 while(num!=0){
                                count++;
			       	num=num/10;
			 }	
                     arr[i]=count;
                }
        
	}

	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int size;
		System.out.println("Enter array size");
		do{
			size=Integer.parseInt(br.readLine());
			if(size<=0){
				System.out.println("Re-Enter array size");
			}
		}while(size<=0);

		int arr[] =new int[size];

		System.out.println("Enter array elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Array");
                for(int i=0;i<arr.length;i++){
                        System.out.print("|"+arr[i]+"|");
                }

		System.out.println();

		CountDigitArr(arr);

		System.out.println("Count of digits in elements of array");
		
                for(int i=0;i<arr.length;i++){
                        System.out.print("|"+arr[i]+"|");
                }
		System.out.println();
	}

}
	

		

