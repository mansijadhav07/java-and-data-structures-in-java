/*
WAP to find a print second min element in array
Input: 2 255 2 1554 15 65 95 89
Output:15
*/

import java.io.*;
class SecondMin{
	public static void main(String[] args)throws IOException{
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));

		int size;
		
		System.out.println("Enter array size");
		do{
		    size=Integer.parseInt(br.readLine());
		    if(size<=0){
			    System.out.println("Re-Enter size of array");
		    }
		}while(size<=0);

		int arr[]=new int[size];

		System.out.println("Enter array elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		int min=arr[0];
		int secondMin=arr[0];

		for(int i=0;i<arr.length;i++){

			if(arr[i]<min){
				secondMin=min;
				min=arr[i];	
			}
			if(arr[i]<secondMin && arr[i]!=min){
				secondMin=arr[i];
			}

		}
		System.out.println("Second minimum element from an array is "+secondMin);
	}
}




