/*
WAP to find a print second max element in array
Input: 2 255 2 1554 15 65
Output:255
*/
import java.io.*;
class SecondMax{
        public static void main(String[] args)throws IOException{
                BufferedReader br =new BufferedReader(new InputStreamReader(System.in));

                int size;

                System.out.println("Enter array size");
                do{
                    size=Integer.parseInt(br.readLine());
                    if(size<=0){
                            System.out.println("Re-Enter size of array");
                    }
                }while(size<=0);

                int arr[]=new int[size];

                System.out.println("Enter array elements");
                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                int max=arr[0];
                int secondMax=arr[0];

                for(int i=0;i<arr.length;i++){
                        if(arr[i]>max){
                                secondMax=max;
                                max=arr[i];
                        }
			if(arr[i]>secondMax && arr[i]!=max){
				secondMax=arr[i];
			}
                }
                System.out.println("Second maximum element from an array is "+secondMax);
        }
}

