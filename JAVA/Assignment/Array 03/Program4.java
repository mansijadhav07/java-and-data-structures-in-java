/*
WAP to find prime number from an array and return index
Take size and elements from user
Input: 10 25 36 566 34 53 50 100
Output:prime no 53 found at index : 5
*/

import java.io.*;
class PrimeNum{
        static void IsPrime(int arr[]){
                for(int i=0;i<arr.length;i++){
                           int num=arr[i];
                            int count=0;
                            if(num==0|| num==1){
                                    count=1;
                            }else{
                                   for(int j=1;j<=num;j++){
                                         if(num % j==0){
                                                count++;
                                        }
                                   }
                                   if(count == 2){
                                           System.out.println("Prime number "+num+" found at index "+i);
                                   }

                          }
                }
        }
	public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int size;
                System.out.println("Enter array size");
                do{
                        size=Integer.parseInt(br.readLine());
                        if(size<=0){
                                System.out.println("Re-Enter array size");
                        }
                }while(size<=0);

                int arr[] =new int[size];

                System.out.println("Enter array elements");
                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                System.out.println("Array");
                for(int i=0;i<arr.length;i++){
                           System.out.print("|"+arr[i]+"|");
                }
                System.out.println();
                IsPrime(arr);
	}
}


