/*
WAP to find a palindrome number from an array and return its index
Input: 10 25 252 36 564
Output:Palindrome no 252 found at index 2
*/
import java.io.*;
class PalindromeNo{
        static void Palindrome(int arr[]){
		for(int i=0;i<arr.length;i++){
			int num=arr[i];
			int temp=num;
			int reverse=0;
			while(num!=0){
				int rem=num%10;
				reverse=reverse*10+rem;
				num=num/10;
			}
			if(temp==reverse){
				System.out.println("Palindrome number "+arr[i]+" found at index "+i);
			}
                }
        }
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int size;
                System.out.println("Enter array size");
                do{
                        size=Integer.parseInt(br.readLine());
                        if(size<=0){
                                System.out.println("Re-Enter array size");
                        }
                }while(size<=0);

                int arr[] =new int[size];

                System.out.println("Enter array elements");
                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                System.out.println("Array");
                for(int i=0;i<arr.length;i++){
                           System.out.print("|"+arr[i]+"|");
                }
                System.out.println();
                Palindrome(arr);
        }
}


