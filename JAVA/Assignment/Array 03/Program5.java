/*
WAP to find a perfect number from an array and return its index
Input:10 25 252 496 564
Output:Perfect no 496 found at index 5
*/
import java.io.*;
class PerfectEle{
	static void PerfectNo(int arr[]){
		for(int i=0;i<arr.length;i++){
			int num=arr[i];
			int sum=0;
			for(int j=1;j<num;j++){
				if(num%j==0){
					sum=sum+j;
				}
			}
			if(sum==arr[i]){
				System.out.println("Perfect no "+arr[i]+" found at index "+i);
			}
		}
	}
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int size;
                System.out.println("Enter array size");
                do{
                        size=Integer.parseInt(br.readLine());
                        if(size<=0){
                                System.out.println("Re-Enter array size");
                        }
                }while(size<=0);

                int arr[] =new int[size];

                System.out.println("Enter array elements");
                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                System.out.println("Array");
                for(int i=0;i<arr.length;i++){
                           System.out.print("|"+arr[i]+"|");
                }
                System.out.println();
                PerfectNo(arr);
        }
}

