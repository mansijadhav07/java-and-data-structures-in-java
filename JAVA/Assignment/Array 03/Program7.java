/*
WAP to find a strong number from an array and return its index
Input: 10 25 252 36 564 145
Output:Strong number 145 found at index 5
*/
import java.io.*;
class StrongNumber{
	static void StrongNo(int arr[]){
		for(int j=0;j<arr.length;j++){
			int num=arr[j];
			int sum=0;
			while(num!=0){
				int rem=num%10;
				int fact=1;
				for(int i=rem;i>0;i--){
					fact=fact*i;
				}
				num=num/10;
				sum=sum+fact;
			}
			if(sum==arr[j]){
				System.out.println("Strong number "+arr[j]+" found at index "+j);
			}

                }
        }
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int size;
                System.out.println("Enter array size");
                do{
                        size=Integer.parseInt(br.readLine());
                        if(size<=0){
                                System.out.println("Re-Enter array size");
                        }
                }while(size<=0);

                int arr[] =new int[size];

                System.out.println("Enter array elements");
                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                System.out.println("Array");
                for(int i=0;i<arr.length;i++){
                           System.out.print("|"+arr[i]+"|");
                }
                System.out.println();
                StrongNo(arr);
        }
}


