/*
WAP to find a Armstrong number from an array and return its index
Input: 10 25 252 36 153 55 89
Output:Armstrong no 153 found at index 4
*/
import java.io.*;
class ArmstrongEle{
	static void ArmstrongNo(int arr[]){
		for(int i=0;i<arr.length;i++){
			int num=arr[i];
			int count=0;
			while(num!=0){
				count++;
				num=num/10;
			}
			int sum=0;
			int temp=arr[i];
			while(temp!=0){
				int rem=temp%10;
				int mult=1;
				for(int j=0;j<count;j++){
				         mult=mult*rem;
				}
				temp=temp/10;
				sum=sum+mult;
			}
			if(sum==arr[i]){
				System.out.println("Armstrong no "+arr[i]+" found at index "+i);
			}
		}
	}


   public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int size;
                System.out.println("Enter array size");
                do{
                        size=Integer.parseInt(br.readLine());
                        if(size<=0){
                                System.out.println("Re-Enter array size");
                        }
                }while(size<=0);

                int arr[] =new int[size];

                System.out.println("Enter array elements");
                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                System.out.println("Array");
                for(int i=0;i<arr.length;i++){
                           System.out.print("|"+arr[i]+"|");
                }
                System.out.println();
                ArmstrongNo(arr);
        }
}

