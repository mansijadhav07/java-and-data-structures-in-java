/*Take an integer N as input
  print multiples of 4 till N
  INPUT:22
  OUTPUT:4 8 12 16 20
*/

import java.util.Scanner;
class Multipleof4{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter value of N");
		int N=sc.nextInt();
		System.out.println("Multiples of 4 till "+N);
                int i=4;
		while(i<=N){
			
			System.out.println(i);
			i=i+4;
		}
	}
}

