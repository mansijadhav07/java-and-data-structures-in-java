//Print the number from 8 to 1
class EightTen{
	public static void main(String[] args){
		int i=8;
		while(i>=1){
			System.out.println(i);
			i--;
		}
	}
}
/* OUTPUT
   8
   7
   6
   5
   4
   3
   2
   1
*/

