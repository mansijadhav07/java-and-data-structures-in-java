//Print Integers from 1 to 10 using while loop

class One_to_Ten{
	public static void main(String[] args){
		int i=1;
		while(i<=10){
			System.out.println(i++);
			//i++;
		}
	}
}
/*OUTPUT
1
2
3
4
5
6
7
8
9
10
*/
