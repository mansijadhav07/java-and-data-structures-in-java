class Hello{
	public static void main(String[] args){
		int i=1;
		while(i<=5){
			System.out.println("HELLO");
		}
	}
}
/* OUTPUT
   HELLO
   HELLO
   .
   .
   .
   INFINITE LOOP

   Because not increment i and condition is always true */
