/* Take two integers A and B as input
   Print the max of two
   Assume:x and y are not equal

INPUT: x=5
       y=7
OUTPUT: 7 is greater*/

import java.util.Scanner;
class maxNum{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter first number");
		int x=sc.nextInt();
		System.out.println("Enter second number");
		int y=sc.nextInt();
		if(x>y){
			System.out.println(x+" is maximum");
		}else{
			System.out.println(y+ " is maximum");
		}
	}
}
