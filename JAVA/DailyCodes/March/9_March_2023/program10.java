/* Electricity Bill problem
   Given an integer input A which represents units of electricity consumed at your name
   Calculate and print the bill amount
   units <= 100  price per unit 1
   units >  100  price per unit 2

   INPUT1:50
   OUTPUT1:50

   INPUT:200
   OUTPUT:300*/

import java.util.Scanner;
class ElectricityBill{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter electricity bill in units");
		int A=sc.nextInt();

		if(A<=100){
			System.out.println("Bill amount is "+A);
		}else{
			int x=A-100;
			int y=100+x*2;
			System.out.println("Bill amount is "+y);
			/* Formula 
			  1) (100+(A-100)*2)
			  2) (A*2)-100
			*/
		}
	}
}
