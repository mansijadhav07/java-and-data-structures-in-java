/* Given the temperature of a person in a farenheit
   Print whether the person has high,normal,low temperature
   >98.6                 --> High
   98.0<= and <=98.6     --> Normal
   <98.0                 -->Low
*/

import java.util.Scanner;
class PersonTemp{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter temperature");
		float temp=sc.nextFloat();

		/*if(temp>98.6f){
			System.out.println("HIGH");
		}else if(temp>=98.0f && temp<=98.6f){
			System.out.println("NORMAL");
		}else if(temp<98.0f){
			System.out.println("LOW");
		}
		 
		if()
		else if()
		is executed successfully without else

		the above code in not optimized because without && we can write correct code
		*/

		if(temp>98.6f){
                        System.out.println("HIGH");
                }else if(temp<98.0f){
                        System.out.println("LOW");
                }else{
                        System.out.println("NORMAL");
                }

	}
}
