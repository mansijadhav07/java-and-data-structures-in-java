/* Given an integer value as input
   Print fizz if it is divisible by 3
   Print buzz if it is divisible by 5
   Print fizzbuzz if it is divisible by both
   If Not then print "Not divisible by both"*/

import java.util.Scanner;
class Divsion{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number");
		int num=sc.nextInt();

		if(num%3==0 && num%5==0){
			System.out.println("Divisible by 3 and 5 both");
		}else if(num%3==0){
			System.out.println("Divisible by 3");
		}else if(num%5==0){
			System.out.println("Divisible by 5");
		}else{
			System.out.println("Not divisible by both");
		}
	}
}
