/* Take two integer A and B as input
   Print the max of two
   INPUT1: x=5 y=7
   OUTPUT1: 7 is greater

   INPUT2:x=5 y=5
   OUTPUT2:Both are equal*/

//if else ladder
import java.util.Scanner;
class greater{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter first number");
		int x=sc.nextInt();
		System.out.println("Enter second number");
		int y=sc.nextInt();

			if(x>y){
				System.out.println(x+" is greater");
			}else if(y>x){
				System.out.println(y+" is greater");
			}else{
				System.out.println("Both are equal");
			}
	}
}
