/* Take an Integer as input and print whether it is divisible by 4 or not
   INPUT:5
   OUTPUT:NOT DIVISIBLE*/

import java.util.Scanner;

class Divby4{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number");
		int num=sc.nextInt();

		if(num%4==0){
			System.out.println("Divisible by 4");
		}else{
			System.out.println("Not divisible by 4");
		}
	}
}
