/*Take N as input
  Print 1 to N
  INPUT:5
  OUTPUT:1 2 3 4 5
*/

class Number{
	public static void main(String[] args){
		int N=5;
		for(int i=1;i<=N;i++){
			System.out.println(i);
		}
	}
}

