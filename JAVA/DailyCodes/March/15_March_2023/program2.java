/*Take N as input
  Print odd integer from 1 to N
*/
class Odd{
	public static void main(String[] args){
		int N=6;
		for(int i=1;i<=N;i++){
			if(i%2==1){
				System.out.println(i);
			}
		}
	}
}
