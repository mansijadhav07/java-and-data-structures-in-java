/* Take N as input
   Print its factorial
   INPUT:5
   OUTPUT:120
*/
class Factorial{
	public static void main(String[] args){
		int N=5;
		int fact=1;
		for(int i=N;i>=1;i--){
			fact=fact*i;
		}
		System.out.println("Factorial of "+N+" is " +fact);
	}
}
