/* Given an integer N
   Print all its digit
   INPUT:6531
   OUTPUT:1
          3
	  5
	  6
*/
class Digits{
	public static void main(String[] args){
		int N=6031;
		if(N==0){
			System.out.println(N);
		}else{

		        while(N!=0){
			        //int rem=num%10;
			        //System.out.println(rem);
		        	System.out.println(N%10);
			        N=N/10;
			}
		}
	}
}
