/*Given an Integer N
  print product of its digits
  Assume:N>=0
  INPUT:135
  OUTPUT:15
*/

class ProductDigit{
	public static void main(String[] args){
		int N=135;
		int product=1;

		while(N!=0){
			int rem=N%10;
			product=product*rem;
			N=N/10;
		}
		System.out.println("Product of digits is ="+product);
		System.out.println("N="+N);
	}
}
