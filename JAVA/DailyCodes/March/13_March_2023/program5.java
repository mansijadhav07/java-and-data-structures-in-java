/*Take an integer N as input
  Print perfect squares till N
  perfect square:An int whose square root is integer*/

class Square{
	public static void main(String[] args){
		int N=30;
	        int i=1;
		//while(i*i<=30)
		while(i<=N){
			if(i*i<=30){
				System.out.println(i*i);
			}
			i++;
		}
	}
}
