/*Take integer N as input print number
  N is perfect number or not
  INPUT:4
  OUTPUT:Not a perfect number

  INPUT:6
  OUTPUT:perfect number
*/
class Isperfect{
	public static void main(String[] args){
		int N=6;
		int sum=0;
		for(int i=1;i<N;i++){
			if(N%i==0){
				sum=sum+i;
			}
		}
		if(sum==N){
			System.out.println(N+" is perfect number");
		}else{
			System.out.println(N+" is not perfect number");
		}
	}
}
