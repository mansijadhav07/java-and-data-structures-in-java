//Take N as input print whether it is prime or not
class Isprime{
	public static void main(String[] args){
		int N=3;
		if(N<0){
			System.out.println(N+ " is not prime number");
		}else if(N==0 || N==1){
			System.out.println(N+ " is not prime nor composite number");
		}else{
			int count=0;
			for(int i=1;i<=N;i++){
				if(N%i==0){
					count++;
				}
			}
			if(count==2){
				System.out.println(N+" is prime number");
			}else{
				System.out.println(N+" is not prime number");
			}
		}
	}
}


 
