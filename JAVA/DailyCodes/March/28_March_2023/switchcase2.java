class SwitchDemo{
	public static void main(String[] args){
		int x=5;
		switch(x){
			case 1:
				System.out.println("1");
			case 2:
				System.out.println("2");
			case 5:
				System.out.println("First-5");
			case 5://error: duplicate case label
				System.out.println("Second-5");
			case 2://error: duplicate case label
				System.out.println("Second-2");
			default:
				System.out.println("NO MATCH");
		}
		System.out.println("After Switch");
	}
}
