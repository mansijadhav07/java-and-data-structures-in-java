class SwitchDemo{
	public static void main(String[] args){
		int ch=65;
		switch(ch){
			case 'A':
				System.out.println("Char A");
				break;
			case 65://duplicate case label
				System.out.println("Number 65");
			        break;
			case 'B':
				System.out.println("Char B");
				break;
			case 66://duplicate case label
				System.out.println("Number 66");
				break;
			default:
				System.out.println("Invalid");
				break;
		}
	}
}
