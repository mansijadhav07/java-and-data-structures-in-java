class SwitchDemo{
	public static void main(String[] args){
		int x=3;
		switch(x){
			case 1:
				System.out.println("ONE");
			case 2:
				System.out.println("TWO");
			case 3:
				System.out.println("THREE");
			case 4:
				System.out.println("FOUR");
			case 5:
				System.out.println("FIVE");
			default:
				System.out.println("NO MATCH");
		}
		System.out.println("After Switch");
	}
}
