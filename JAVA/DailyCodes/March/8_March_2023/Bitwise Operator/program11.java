/*The meaning of ternary is composed of three parts. 
The ternary operator (? :) consists of three operands. 
It is used to evaluate Boolean expressions. 
The operator decides which value will be assigned to the variable. 
It is the only conditional operator that accepts three operands. 
It can be used instead of the if-else statement. 
It makes the code much more easy, readable, and shorter.*/
class Ternary{
	public static void main(String[] args){
		int x=10;
		int y=20;

		System.out.println((x<y)?x:y);//10
		/* if(x<y)
		     System.out.println(x);
		   else
		     System.out.println(y);
		*/
		System.out.println((x>y)?x:y);//20
	}
}
