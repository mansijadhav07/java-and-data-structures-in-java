/*we are going to perform a Zero fill Right shift on -5 (in binary 11111111111111111111111111111011) by 2 bits.

Step 1 − Before Right Shift
1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	0 	1 	1

Step 2 − After Right Shift, the last 2 bits (11) will be discarded and two zeros will be inserted on the left side, which are the replacement bits.
0 	0 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	1 	0

Now the new number is 1073741822, which is positive.*/
class Operator{
	public static void main(String[] args){
		int x=-7;
		System.out.println("x>>2="+(x>>2));
		System.out.println("x>>>2="+(x>>>2));
	}
}
