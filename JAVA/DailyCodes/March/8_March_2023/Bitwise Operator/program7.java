/*Bitwise Complement (~)

This operator is a unary operator, denoted by ‘~.’ 
It returns the one’s complement representation of the input value, 
i.e., with all bits inverted,
which means it makes every 0 to 1, and every 1 to 0. 
*/
class Negation{
	public static void main(String[] args){
		int x=-195;
		System.out.println(~x);//194
	}
}
