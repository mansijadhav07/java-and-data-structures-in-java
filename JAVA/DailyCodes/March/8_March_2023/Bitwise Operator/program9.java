//>>> bitwise complement operator
//It is the same as the signed right shift,when value is positive
class Operator{
	public static void main(String[] args){
		int x=7;
		System.out.println(x>>2);//1
		System.out.println(x>>>2);//1
	}
}
