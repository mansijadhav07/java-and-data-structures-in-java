/* SCOPE OF VARIABLE
   Useful lifetime of variable known as scope of variable*/

//Q.Predict the output
class Core2Web{
	public static void main(String[] args){
		int x=10;
		{
			int x=20;//error1
			System.out.println(x);
		}
		{
			int x=30;//error2
			System.out.println(x);
		}
		System.out.println(x);
	}
}
/*OUTPUT
error: variable x is already defined in method main(String[])
			int x=20;
			    ^
error: variable x is already defined in method main(String[])
			int x=30;
			    ^
2 errors*/


