/* SCOPE OF VARIABLE
   Useful lifetime of variable known as scope of variable*/

//Q.Predict the output
class Core2Web{
	public static void main(String[] args){
		int x=10;
		{
			int y=20;
			System.out.println(x+ " "+y);
		}
		{
		        int y=15;
			System.out.println(x+ " "+y);
		}
		System.out.println(x+ " "+y);//error:cannot find symbol
	}
}

