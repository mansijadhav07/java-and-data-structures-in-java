/* SCOPE OF VARIABLE
   Useful lifetime of variable known as scope of variable*/

//Q.Predict the output
class Core2Web{
	public static void main(String[] args){
		int x=10,y=20;
		{
			System.out.println(x+ " "+y);
		}
		{
			x=15;//assignment
			System.out.println(x+ " "+y);
		}
		System.out.println(x+ " "+y);
	}
}
/* OUTPUT
   10 20
   15 20
   15 20*/

