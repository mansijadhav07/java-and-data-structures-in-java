/* SCOPE OF VARIABLE
   Useful lifetime of variable known as scope of variable*/

//Q.Predict the output
class Core2Web{
	public static void main(String[] args){
		int x=10;
		{       
			int y=20;
			System.out.println(x+ " "+y);
		}
		{
			System.out.println(x+ " "+y);//error:cannot find symbol
		}
		System.out.println(x+ " "+y);//error:cannot find symbol
	}
}
/* OUTPUT
   program2.java:13: error: cannot find symbol
			System.out.println(x+ " "+y);
			                          ^
  symbol:   variable y
  location: class Core2Web
  program2.java:15: error: cannot find symbol
		System.out.println(x+ " "+y);
		                          ^
  symbol:   variable y
  location: class Core2Web
2 errors
*/

