class Parent{
	Parent(){
	        System.out.println("Address of obj/this in Parent constructor="+this);
		System.out.println("In Parent Constructor");
	}
	void parentProperty(){
		System.out.println("Flat,Car,Gold");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("Address of obj/this in child constructor="+this);
		System.out.println("In Child Constructor");
	}
}
class Client{
	public static void main(String[] args){
		Child obj=new Child();
		System.out.println("Address of obj in client="+obj);
		obj.parentProperty();
	}
}
/*
OUTPUT
Address of obj/this in Parent constructor=Child@8bcc55f
In Parent Constructor
Address of obj/this in child constructor=Child@8bcc55f
In Child Constructor
Address of obj in client=Child@8bcc55f
Flat,Car,Gold
*/

