class Unix{
	Unix(){
		System.out.println("UNIX Operating System-1970");
	}
	void Develop(){
		System.out.println("Unix Written by bell labs");
	}
}
class Linux extends Unix{
	Linux(){
		System.out.println("LINUX Operating System-1991");
	}
}
class Client{
	public static void main(String[] args){
		Linux obj=new Linux();
		obj.Develop();
	}
}
