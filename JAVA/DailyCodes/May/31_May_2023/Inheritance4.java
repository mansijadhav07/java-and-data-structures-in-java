class Parent{
        Parent(){
                System.out.println("In Parent Constructor");
        }
        void parentProperty(){
                System.out.println("Flat,Car,Gold");
        }
}
class Child extends Parent{
        Child(){
                System.out.println("In Child Constructor");
        }
}
class Client{
        public static void main(String[] args){
                Parent obj1=new Child();
                Object obj2=new Child();
		//Child obj3=new Parent();
		//error: incompatible types: Parent cannot be converted to Child
        }
}
/*
In Parent Constructor
In Child Constructor
In Parent Constructor
In Child Constructor
*/

