class Player{
	int jerNo=18;
	String name="Virat";

	Player(){
		System.out.println("In Constructor");
	}
	void info(){
		System.out.println(jerNo+"="+name);
	}
}
class Client{
	public static void main(String[] args){
	Player obj1=new Player();
	obj1.info();

	Player obj2=new Player();
	obj2.jerNo=7;
	obj2.name="MSD";
	obj2.info();

	Player obj3=new Player();
	obj3.info();
	}
}
/*
In Constructor
18=Virat
In Constructor
7=MSD
In Constructor
18=Virat
*/
