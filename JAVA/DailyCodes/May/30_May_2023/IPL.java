/*public class Demo{

}
error: class Demo is public, should be declared in a file named Demo.java*

private class IPL{

}
error: modifier private not allowed here

protected class IPL{

}
error: modifier protected not allowed here

static class IPL{

}
error: modifier static not allowed here

*/
public class IPL{

}

