class Player{
	private int jerNo=0;
	private String name=null;
	Player(int jerNo,String name){
		this.jerNo=jerNo;
		this.name=name;
		System.out.println("IN CONSTRUCTOR");
	}
	void info(){
		System.out.println(jerNo+"="+name);
		System.out.println(System.identityHashCode(name));
	}
}
class Client{
	public static void main(String[] args){
		Player obj1=new Player(18,"Virat");
		obj1.info();
		Player obj2=new Player(18,"Virat");
                obj2.info();
	}
}


