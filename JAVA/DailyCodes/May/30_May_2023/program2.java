class Player{
        private int jerNo=0;
	private String name=null;
	Player(){
		System.out.println("In Constructor");
	}
	void info(){
		System.out.println(jerNo+"="+name);
	}
}
class Client{
	public static void main(String[] args){
		Player obj1=new Player();
		obj1.info();
		Player obj2=new Player();
		//obj2.jerNo=7;error: jerNo has private access in Player
		//obj2.name="MSD";error: name has private access in Player
		obj2.info();

	}
}


