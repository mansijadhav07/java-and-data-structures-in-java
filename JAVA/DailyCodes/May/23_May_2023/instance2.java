class Demo{
	static int x=10;
	void fun(){
		System.out.println(x);
	}
	void gun(){
		fun();
	}
	public static void main(String[] args){
		//gun();
		//error: non-static method gun() cannot be referenced from a static context
	        Demo obj=new Demo();
		obj.gun();

	}
}
