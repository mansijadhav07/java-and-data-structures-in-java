class Demo{
	static int x=10;//Java Supports global static variable
	
	//All below is not allowed in java
	static{
		static int y=20;
	}
	static void fun(){
		static int z=30;
	}
	static void gun(){
		static int z=20;
	}
}
