class Demo{
        static{
                System.out.println("Static Block 1");
        }
}
class Client{
        static{
                System.out.println("Static Block 2");
        }
        public static void main(String[] args){
                System.out.println("In Client main");
        }
        static{
                System.out.println("Static Block 3");
        }
}
/*
java Client
Static Block 2
Static Block 3
In Client main

because static block of one class cannot be inherited from one class to another


java Demo
Error: Main method not found in class Demo, please define the main method as:
   public static void main(String[] args)
or a JavaFX application class must extend javafx.application.Application
*/
