class ArrayDemo{
	public static void main(String[] args){
               //int arr[5];//error: ']' expected
	       //int arr[]=new int[];// error: array dimension missing

	         int arr1[]=new int[5];
		 int arr2[]=new int[]{10,20,30,40,50};
		 int arr3[]={100,200,300,400,500};

	}
}
