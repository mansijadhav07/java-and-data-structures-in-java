class ArrayDemo{
        static	int a;
        static  float b;
        static  char c;
        static  double d;
        static  boolean t;
	static String str;
	static long l;

	public static void main(String[] args){
		int arr1[];
		int[] arr2;
		/* int a;error: variable a might not have been initialized
	             	System.out.println(a);

	           float b; error:variable b might not have been initialized
		           System.out.println(b);
		                   
                   char c;error: variable c might not have been initialized
		           System.out.println(c);
                   double d; error: variable d might not have been initialized
	                     System.out.println(d);
	           boolean b;error: variable t might not have been initialized
		             System.out.println(t);
		*/
		System.out.println("Default value of int ="+a);
		System.out.println("Default value of float="+b);
		System.out.println("Default value of char="+c);
	        System.out.println("Default value of double="+d);
		System.out.println("Default value of string="+str);
		System.out.println("Default value of boolean="+t);
                System.out.println("Defaut value of long="+l);

		char ch='\u0000';
		System.out.println("ch="+ch);
	}
}
