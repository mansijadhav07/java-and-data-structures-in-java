//Default array values in java
class ArrayDemo{
	public static void main(String[] args){

		int narr[]=new int[4];
		System.out.println("Integer array default values");
		for(int i=0;i<=3;i++){
			System.out.println(narr[i]);
		}

		char carr[]=new char[4];
                System.out.println("Character array default values");
                for(int i=0;i<=3;i++){
                        System.out.println(carr[i]);
                }

		float farr[]=new float[4];
                System.out.println("float array default values");
                for(int i=0;i<=3;i++){
                        System.out.println(farr[i]);
                }

		double darr[]=new double[4];
                System.out.println("Double array default values");
                for(int i=0;i<=3;i++){
                        System.out.println(darr[i]);
                }

		String sarr[]=new String[4];
                System.out.println("String array default values");
                for(int i=0;i<=3;i++){
                        System.out.println(sarr[i]);
                }

		boolean barr[]=new boolean[4];
                System.out.println("boolean  array default values");
                for(int i=0;i<=3;i++){
                        System.out.println(barr[i]);
                }

		ArrayDemo obj[]=new ArrayDemo[4];
		System.out.println("object array default values");
                for(int i=0;i<=3;i++){
                        System.out.println(obj[i]);
                }
      
	}
}
