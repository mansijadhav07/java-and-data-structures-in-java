class ArrayDemo{

	void fun(int[] arr){
		arr[1]=5000;
		arr[2]=6000;
		System.out.println("arr in fun="+System.identityHashCode(arr));

		System.out.println("arr[0]="+System.identityHashCode(arr[0]));
                System.out.println("arr[1]="+System.identityHashCode(arr[1]));
                System.out.println("arr[2]="+System.identityHashCode(arr[2]));
                System.out.println("arr[3]="+System.identityHashCode(arr[3]));

	}
	public static void main(String[] args){
		int arr[]={1000,2000,3000,4000};

        	System.out.println("arr in main="+System.identityHashCode(arr));

		System.out.println("arr[0]="+System.identityHashCode(arr[0]));
		System.out.println("arr[1]="+System.identityHashCode(arr[1]));
		System.out.println("arr[2]="+System.identityHashCode(arr[2]));
		System.out.println("arr[3]="+System.identityHashCode(arr[3]));

		ArrayDemo obj=new ArrayDemo();

		obj.fun(arr);

		for(int x:arr){
			System.out.println(x);
		}

		int x=70;
		int y=80;

		System.out.println("x="+System.identityHashCode(x));
                System.out.println("y="+System.identityHashCode(y));
	}
}

