class IntegerCache{
	public static void main(String[] args){
		/* Two different unique id return because Integer class maintains the Integer Cache for integers which by default
		   ranges from -128 to 127*/
		int a=128;
		int b=128;
		System.out.println(System.identityHashCode(a));//1933863327

	        System.out.println(System.identityHashCode(b));//112810359


		int x=12;
                int y=12;
                System.out.println(System.identityHashCode(x));//345826525
                                                              
		System.out.println(System.identityHashCode(y));//345826525
							 
		Integer c=127;
		Integer d=127;

		System.out.println(System.identityHashCode(x));//345826525

                System.out.println(System.identityHashCode(y));//345826525
							 
		Integer e=128;
                Integer f=128;

                System.out.println(System.identityHashCode(e));//2124308362

                System.out.println(System.identityHashCode(f));//146589023
							       
	        Integer obj1=Integer.valueOf(127);
                Integer obj2=127;

                System.out.println(System.identityHashCode(obj1));//238686549

                System.out.println(System.identityHashCode(obj2));//238686549


	}
}
