class ForEach{
	public static void main(String[] args){
		int arr[]={10,20,30,40,50};

		System.out.println("Accessing array  element using for each loop");
                for(int x:arr){
                        System.out.print("|"+x+"|");//10
			break;
                }
                System.out.println();

		
                for(int x:arr){
                        System.out.print("|"+x+"|");//10 20 30
			if(x==30){
                              break;
			}
                }
                System.out.println();

	}
}

