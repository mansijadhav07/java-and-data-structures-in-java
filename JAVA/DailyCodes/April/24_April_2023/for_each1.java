class ForEach{
	public static void main(String[] args){
		int arr[]={10,20,30,40,50};

		System.out.println("Accessing array  element using for loop");
		for(int i=0;i<arr.length;i++){
			System.out.print("|"+arr[i]+"|");
		}
		System.out.println();

		System.out.println("Accessing array  element using for each loop");
                for(int x:arr){
                        System.out.print("|"+x+"|");
                }
                System.out.println();
	}
}

