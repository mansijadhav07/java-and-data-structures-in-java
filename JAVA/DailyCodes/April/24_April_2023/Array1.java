class ArrayDemo{
	public static void main(String[] args){
		int arr1[]={100,200,300,400};

		byte arr2[]={1,2,3,4};

		short arr3[]={20,30,40,50};

		boolean arr4[]={true,false};

		//float arr5[]={10.5,20.5,30.5};
		//System.out.println(arr5);
		//error: incompatible types: possible lossy conversion from double to float
                  
		double arr6[]={10d,20d,30d};//d means double


		System.out.println(arr1);//[I@7344699f
                                       
		System.out.println(arr2);//[B@6b95977
              
		System.out.println(arr3);//[S@7e9e5f8a

		System.out.println(arr4);//[Z@8bcc55f

		System.out.println(arr6);//[D@58644d46

	}
}

