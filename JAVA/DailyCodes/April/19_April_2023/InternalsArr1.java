class Array{
	public static void main(String[] args){
		//integer array
		int arr1[]={10,20,30,40};
		int arr2[]={10,20,30,40};
		System.out.println("Integer Array");

		System.out.println(arr1);
		System.out.println(arr2);

		System.out.println(System.identityHashCode(arr1[0]));
                System.out.println(System.identityHashCode(arr2[0]));



                //float array
		float arr3[]={10.5f,20.5f,30.5f,40.5f};
		float arr4[]={10.5f,20.5f,30.5f,40.5f};

		System.out.println("float Array");

		System.out.println(arr3);
                System.out.println(arr4);

		System.out.println(System.identityHashCode(arr3[0]));
                System.out.println(System.identityHashCode(arr4[0]));


                //character array
                char arr5[]={'A','B','C'};
                char arr6[]={'A','B','C'};

                System.out.println("Character Array");

		System.out.println(arr5);
                System.out.println(arr6);

		System.out.println(System.identityHashCode(arr5[0]));
                System.out.println(System.identityHashCode(arr6[0]));

	}
}
