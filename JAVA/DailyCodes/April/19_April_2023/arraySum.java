import java.io.*;
class ArraySum{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Size of an array");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		int sum=0;
		System.out.println("Enter array elements");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
			sum=sum+arr[i];
		}
		System.out.println("Sum of array elements is "+sum);
	}
}
