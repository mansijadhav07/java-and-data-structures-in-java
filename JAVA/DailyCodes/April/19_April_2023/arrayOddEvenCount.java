import java.io.*;

class Array{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Size of an array");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];
                int evenCount=0;
		int oddCount=0;
                System.out.println("Enter array elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                        if(arr[i]%2==0){
                                   evenCount++;
                        }else{
				oddCount++;
			}
		}
                System.out.println("Count of even array elements is "+evenCount);
		System.out.println("Count of odd array elements is "+oddCount);

        }
}
           
