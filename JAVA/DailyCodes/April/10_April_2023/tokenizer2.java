import java.io.*;
import java.util.*;
class StudentInfo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter student name,division,roll no and percentage");
		String info=br.readLine();

		StringTokenizer obj=new StringTokenizer(info," ");

		String stname=obj.nextToken();
		char div=(obj.nextToken()).charAt(0);
		//the nextToken method returns a string which is not compatible with char 
		//to read one char charAt(0) is used
		int roll=Integer.parseInt(obj.nextToken());
		float percent=Float.parseFloat(obj.nextToken());

		System.out.println("Student name:"+stname);
		System.out.println("Division:"+div);
		System.out.println("Roll number:"+roll);
		System.out.println("Percentage:"+percent);

	}
}
