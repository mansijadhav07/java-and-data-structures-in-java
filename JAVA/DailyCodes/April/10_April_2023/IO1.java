import java.io.*;
class IODemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br1=new BufferedReader(new InputStreamReader(System.in));
		BufferedReader br2=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter string1");
		String str1=br1.readLine();
		System.out.println("String="+str1);
		br1.close();
		//BufferedReader br2=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter string2");
		String str2=br2.readLine();
		System.out.println("String2="+str2);
	}
}
/*
Enter string1
mansi
String=mansi
Enter string2
Exception in thread "main" java.io.IOException: Stream closed
	at java.base/java.io.BufferedInputStream.getBufIfOpen(BufferedInputStream.java:168)
	at java.base/java.io.BufferedInputStream.read(BufferedInputStream.java:334)
	at java.base/sun.nio.cs.StreamDecoder.readBytes(StreamDecoder.java:270)
	at java.base/sun.nio.cs.StreamDecoder.implRead(StreamDecoder.java:313)
	at java.base/sun.nio.cs.StreamDecoder.read(StreamDecoder.java:188)
	at java.base/java.io.InputStreamReader.read(InputStreamReader.java:177)
	at java.base/java.io.BufferedReader.fill(BufferedReader.java:162)
	at java.base/java.io.BufferedReader.readLine(BufferedReader.java:329)
	at java.base/java.io.BufferedReader.readLine(BufferedReader.java:396)
	at IODemo.main(IO1.java:11)

*/
