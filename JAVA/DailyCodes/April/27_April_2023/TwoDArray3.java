//Initialization of 2D array
class TwoDArr{
	public static void main(String[] args){
		int arr[][]=new int[3][];//no error because java thinks it is null array of 3 rows this is why java allow jagged Array

		int arr[][]=new int[][];//error:array dimension missing
		
		int arr[][]=new int[][3];//error: ']' expected
	}
}
