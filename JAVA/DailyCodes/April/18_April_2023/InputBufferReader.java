//Take array from user using BufferedReader
import java.io.*;
class InputDemo{
	public static void main(String[] args)throws IOException{
		int arr[] = new int[4];
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array elements");
		for(int i=0;i<4;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Array elements");
		for(int i=0;i<4;i++){
			System.out.println(arr[i]);
		}
	}
}
