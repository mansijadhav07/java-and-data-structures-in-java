class Demo{
	int x=10;
	static int y=20;
	void fun(){
		System.out.println("In fun method");
		System.out.println(x);
		System.out.println(y);
	}
	public static void main(String[] args){
		Demo obj=new Demo();
		obj.fun();
		//System.out.println(x);// non-static variable x cannot be referenced from a static context
		System.out.println(obj.x);
                System.out.println(y);
	}
}
