class ArrayDemo{
	static void fun(int xarr[]){
		System.out.println("In fun");
		for(int x:xarr){
			System.out.print("|"+x+"|");//|10||20||30|
		}
		System.out.println();
	}
	public static void main(String[] args){
		int arr[]={10,20,30};

		System.out.println("In main");

		for(int x:arr){
			System.out.print("|"+x+"|");//|10||20||30|
		}
		System.out.println();

		fun(arr);
	}
}

