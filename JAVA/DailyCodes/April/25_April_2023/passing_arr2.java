class ArrayDemo{
	static void fun(int xarr[]){

		System.out.println("In fun");

		System.out.println("In fun address of array="+xarr);

		for(int x:xarr){
			System.out.print("|"+x+"|");//|10||20||30|
		}
		System.out.println();

		xarr[0]=50;
	}
	public static void main(String[] args){
		int arr[]={10,20,30};

		System.out.println("In main");

	        System.out.println("In main address of array="+arr);

		for(int x:arr){
			System.out.print("|"+x+"|");//|10||20||30|
		}
		System.out.println();

		fun(arr);

		System.out.println("In main after fun call address of array="+arr);

		System.out.println("In main after changing 0 index in fun method");

		for(int x:arr){
                        System.out.print("|"+x+"|");//|50||20||30|
                }
                System.out.println();

	}
}

