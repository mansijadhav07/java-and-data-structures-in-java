class Demo{
	fun (int x){//we didn't give return type
		System.out.println("In fun");
		System.out.println(x);
	}
	public static void main(String[] args){
		System.out.println("In main");
		Demo obj=new Demo();
		obj.fun();
	}
}
/*
error: invalid method declaration; return type required
	fun (int x){
	^
1 error
*/
