class Demo{
	void fun(int x){
		System.out.println("IN FUN");
	}
	public static void main(String[] args){
		Demo obj=new Demo();
		obj.fun();//error
	}
}
/*
error: method fun in class Demo cannot be applied to given types;
		obj.fun();//error
		   ^
  required: int
  found: no arguments
  reason: actual and formal argument lists differ in length
1 error
*/
