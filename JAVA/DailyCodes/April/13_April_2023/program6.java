class Demo{
	void fun(int x){
		System.out.println("In Fun");
		System.out.println("x="+x);
	}
	public static void main(String[] args){
		Demo obj=new Demo();
		obj.fun(10);
		obj.fun(10.5f);
	}
}
/*
error: incompatible types: possible lossy conversion from float to int
		obj.fun(10.5f);
*/
//JAVA doesn't allow for data loss
