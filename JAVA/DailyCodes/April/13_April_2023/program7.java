class Demo{
	void fun(float x){
		System.out.println("In Fun");
		System.out.println("x="+x);
	}
	public static void main(String[] args){
		Demo obj=new Demo();
		obj.fun(10);
		obj.fun(10.5f);
		obj.fun('A');
		//obj.fun(true);
	}
}
/*
OUTPUT
In Fun
x=10.0
In Fun
x=10.5
In Fun
x=65.0
*/
/*
error: incompatible types: boolean cannot be converted to float
		obj.fun(true);
		        ^
Note: Some messages have been simplified; recompile with -Xdiags:verbose to get full output
1 error

type on terminal:javac -Xdiags:verbose program7.java

error: method fun in class Demo cannot be applied to given types;
		obj.fun(true);
		   ^
  required: float
  found: boolean
  reason: argument mismatch; boolean cannot be converted to float
1 error
*/

