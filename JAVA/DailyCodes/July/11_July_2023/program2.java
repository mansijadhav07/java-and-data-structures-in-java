class Outer{
	int x=10;
	static int y=20;

	class Inner{
		int a=30;
	        static int b=40;
	}
}
/*
error: Illegal static declaration in inner class Outer.Inner
		static int b=40;
		           ^
  modifier 'static' is only allowed in constant variable declarations
*/
