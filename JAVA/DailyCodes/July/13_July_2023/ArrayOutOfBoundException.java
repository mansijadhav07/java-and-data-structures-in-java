class ArrayDemo{
	public static void main(String[] args){
		int arr[]=new int[]{10,20,30,40,50};
		for(int i=0;i<=arr.length;i++){
			System.out.println(arr[i]);
		}
	}
}
/*
on-15-5510:~/JAVA DataStructures/java-and-data-structures-in-java/JAVA/DailyCodes/July/13_July_2023$ java ArrayDemo 
10
20
30
40
50
Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Index 5 out of bounds for length 5
*/
