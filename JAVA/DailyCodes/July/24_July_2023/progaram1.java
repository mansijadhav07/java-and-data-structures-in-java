//Concurrency methods in thread class
//sleep()
class MyThread extends Thread{
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class ThreadDemo{
	public static void main(String[] args)throws InterruptedException{
		System.out.println(Thread.currentThread()); 
		MyThread obj=new MyThread();
		obj.start();
		Thread.sleep(1000);
		Thread.currentThread().setName("Mansi");
		System.out.println(Thread.currentThread()); 
	}
}
/*
OUTPUT
Thread[main,5,main]                                                                                                           Thread[Thread-0,5,main]                                                                                                       Thread[Mansi,5,main] 
*/
