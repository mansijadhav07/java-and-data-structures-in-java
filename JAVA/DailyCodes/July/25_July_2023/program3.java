class MyThread extends Thread{
	MyThread(String str){
		super(str);
	}
	MyThread(){
		super();
	}
	public void run(){
		System.out.println(getName());
		System.out.println(Thread.currentThread().getThreadGroup());
	}
}
class ThreadgroupDemo{
	public static void main(String[] args){
		MyThread obj1=new MyThread("Mansi");
		obj1.start();


		MyThread obj2=new MyThread("Jadhav");   
		obj2.start();

		 MyThread obj3=new MyThread();
		 obj3.start();

	}
}
/*
OUTPUT
Mansi                                                                                                                         Thread-0                                                                                                                      java.lang.ThreadGroup[name=main,maxpri=10]                                                                                    Jadhav                                                                                                                        java.lang.ThreadGroup[name=main,maxpri=10]                                                                                    java.lang.ThreadGroup[name=main,maxpri=10]    
*/
