class MyThread extends Thread{
	MyThread(String str){
		super(str);
	}
	public void run(){
		System.out.println(getName());
		System.out.println(Thread.currentThread().getThreadGroup());  
	}
}
class ThreadgroupDemo{
	public static void main(String[] args){
		MyThread obj=new MyThread("Mansi");
		obj.start();
	}
}
/*
OUTPUT
Mansi                                                                                                                         java.lang.ThreadGroup[name=main,maxpri=10]  
*/
