class MyThread extends Thread{
	MyThread(ThreadGroup tg,String str){
             super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class ThreadgroupDemo{
	public static void main(String[] args){
		ThreadGroup pthreadGP=new ThreadGroup("Unix");
		MyThread obj1=new MyThread(pthreadGP,"Linux");
		MyThread obj2=new MyThread(pthreadGP,"MacOS");
		MyThread obj3=new MyThread(pthreadGP,"Windows");
		obj1.start();
		obj2.start();
		obj3.start();

	}
}
/*
OUTPUT
Thread[Linux,5,Unix]                                                                                                          Thread[Windows,5,Unix]                                                                                                        Thread[MacOS,5,Unix] 
*/
