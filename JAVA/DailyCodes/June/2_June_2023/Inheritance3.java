class Parent{
	static{
		System.out.println("In Parent static block");
	}
}
class Child extends Parent{
	static{
		System.out.println("In Child static block");
	}
}
class Client{
	public static void main(String[] args){

	}
}
/*
No output of above code because client is third party class 
if we want to access other class we have to mention that class name*/ 
