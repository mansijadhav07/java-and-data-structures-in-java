class Parent{
	Parent(){
		System.out.println("In Parent Constructor");
	}
}
class Child extends Parent{
	Child(){
		//super();with or without super bytecode is same so we conclude super() is equivalent to invokespecial
		System.out.println("In Child Constructor");
	}
}
class Client{
	public static void main(String[] args){
		Child obj=new Child();
	}
}
