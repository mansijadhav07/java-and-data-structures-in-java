class Parent{
	int x=10;
	static int y=20;
	Parent(){
		System.out.println("Parent Constructor");
	}
}
class Child extends Parent{
	int x=100;
	static int y=200;
	Child(){
		System.out.println("Child Constructor");
	}
	void access(){
		System.out.println(this);

		System.out.println(super.x);//10
                System.out.println(super.y);//20
					    
	//	System.out.println(Parent.x);//error
                System.out.println(Parent.y);//20

		System.out.println(this.x);//100
                System.out.println(this.y);//200

		System.out.println(x);//100
		System.out.println(y);//200
	}
}
class Client{
	public static void main(String[] args){

		Child obj=new Child();
		System.out.println(obj);

		obj.access();
	}
}

