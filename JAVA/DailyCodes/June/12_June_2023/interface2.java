interface Demo{
	void fun();
	void gun();
}
class DemoChild implements Demo{
	void fun(){
		System.out.println("In fun");
	}
	void gun(){
		System.out.println("In gun");
	}
}
/*
interface2.java:9: error: gun() in DemoChild cannot implement gun() in Demo
	void gun(){
	     ^
  attempting to assign weaker access privileges; was public
interface2.java:6: error: fun() in DemoChild cannot implement fun() in Demo
	void fun(){
	     ^
  attempting to assign weaker access privileges; was public
2 errors

because in interface methods are public abstract we are reducing scope public to default in child 
*/
