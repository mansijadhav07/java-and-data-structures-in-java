//multiple inheritace is not allowed in java 
class Demo1{
	void fun(){
		System.out.println("In Demo1 fun");
	}
}
class Demo2{
	void fun(){
		System.out.println("In Demo2 fun");
	}
}
class DemoChild extends Demo2,Demo1{
}
