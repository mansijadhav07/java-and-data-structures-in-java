class Parent{
	Parent(){
		System.out.println("Parent Constructor");
	}
	void Property(){
		System.out.println("Home,car,gold");
	}
	void marry(){
                System.out.println("Pooja Bhatt");
        }
}
class Child extends Parent{
	Child(){
		System.out.println("Child Constructor");
	}
	void marry(){
		System.out.println("Alia Bhatt");
	}
}
class Client{
	public static void main(String[] args){
		Child obj=new Child();
		obj.Property();
		obj.marry();
	}
}
/*
OUTPUT
Parent Constructor
Child Constructor
Home,car,gold
Alia Bhatt
*/

