class Parent{
        Parent(){
                System.out.println("Parent Constructor");
        }
        void fun(){
                System.out.println("In Parent fun");
        }
}
class Child extends Parent{
        Child(){
                System.out.println("Child Constructor");
        }
        void fun(int x){
                System.out.println("In Child fun");
        }
}
class Client{
        public static void main(String[] args){
                Parent obj=new Child();
                obj.fun(20);
        }
}
/*
error: method fun in class Parent cannot be applied to given types;
                obj.fun(20);
                   ^
  required: no arguments
  found: int
  reason: actual and formal argument lists differ in length
1 error
*/
