class Parent{
        void fun(){
                System.out.println("Parent fun");
        }
}
class Child extends Parent{
        static void fun(){
                System.out.println("Child fun");
        }
}
class Client{
        public static void main(String[] args){
                Parent obj=new Child();
                obj.fun();
        }
}
/*
error: fun() in Child cannot override fun() in Parent
        static void fun(){
                    ^
  overriding method is static
*/
