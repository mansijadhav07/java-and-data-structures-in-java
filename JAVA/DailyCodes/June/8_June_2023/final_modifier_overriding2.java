class Parent{
        void fun(){
                System.out.println("Parent fun");
        }
}
class Child extends Parent{
        final void fun(){
                System.out.println("Child fun");
        }
}
class Client{
        public static void main(String[] args){
                Parent obj=new Child();
                obj.fun();

	        Parent obj1=new Parent();
                obj1.fun();

        }
}
/*
OUTPUT
Child fun
Parent fun
*/
