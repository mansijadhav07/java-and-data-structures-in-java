class Parent{
        private void fun(){
                System.out.println("Parent fun");
        }
}
class Child extends Parent{
        private void fun(){
                System.out.println("Child fun");
        }
}
class Client{
        public static void main(String[] args){
                Parent obj1=new Parent();
                obj1.fun();

		Child obj2=new Child();
                obj2.fun();
        }
}
/*
error: fun() has private access in Parent
                obj1.fun();
                    ^
error: fun() has private access in child
                obj2.fun();
                    ^
2 errors
*/


