class Parent{
        void fun(){
                System.out.println("Parent fun");
        }
}
class Child extends Parent{
        private void fun(){
                System.out.println("Child fun");
        }
}
class Client{
        public static void main(String[] args){
                Parent obj=new Child();
                obj.fun();
        }
}
/*
error: fun() in Child cannot override fun() in Parent
        private void fun(){
                     ^
attempting to assign weaker access privileges; was package
*/

