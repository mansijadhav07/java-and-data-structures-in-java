class Singletone{
	static Singletone obj=new Singletone();
	private Singletone(){
		System.out.println("Constructor");
	}
	static Singletone getObject(){
		return obj;
	}
}
class Client{
	public static void main(String[] args){
		Singletone.getObject();
		Singletone obj1=Singletone.getObject();
		System.out.println(obj1);

		Singletone obj2=Singletone.getObject();
                System.out.println(obj2);

		Singletone obj3=Singletone.getObject();
                System.out.println(obj3);
	}
}
/*
OUTPUTConstructor
Singletone@6b95977
Singletone@6b95977
Singletone@6b95977
*/


