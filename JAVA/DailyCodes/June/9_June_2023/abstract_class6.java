abstract class Parent{
        void career(){
                System.out.println("Doctor");
        }
        void marry(){
		System.out.println("Nupoor Sanon");
	}
}
class Child extends Parent{
        void marry(){
                System.out.println("Kriti Sanon");
        }
}
class Client{
        public static void main(String[] args){
                Child obj=new Child();
                obj.career();
                obj.marry();
        }
}

