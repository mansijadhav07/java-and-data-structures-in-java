class Demo{
        void fun(int x){
                System.out.println("Int para");
        }
        void fun(float f){
                System.out.println("Char para");
        }
}
class Client{
        public static void main(String[] args){
                Demo obj=new Demo();
                obj.fun(10.5);
        }
}
/*
error: no suitable method found for fun(double)
                obj.fun(10.5);
                   ^
    method Demo.fun(int) is not applicable
      (argument mismatch; possible lossy conversion from double to int)
    method Demo.fun(float) is not applicable
      (argument mismatch; possible lossy conversion from double to float)
*/

