import java.io.*;
class BubbleSort{
        void bubbleSort(int[] arr){

                for(int i=0;i<arr.length-1;i++){
                        boolean swapped=false;
                        for(int j=0;j<arr.length-i-1;j++){
                                if(arr[j]>arr[j+1]){
                                        int temp=arr[j];
                                        arr[j]=arr[j+1];
                                        arr[j+1]=temp;
                                        swapped=true;
                                }
                        }
                        if(swapped==false){
                                break;
                        }
                }
        }
        void printArr(int[] arr){
                for(int i=0;i<arr.length;i++){
                        System.out.print(arr[i]+" ");
                }
                System.out.println();
        }
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader( new InputStreamReader(System.in));
                System.out.println("Enter size");
                int size=Integer.parseInt(br.readLine());
                int[] arr=new int[size];
                System.out.println("Enter array elements");
                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                BubbleSort obj=new BubbleSort();
                obj.printArr(arr);
                obj.bubbleSort(arr);
                System.out.println("Sorted Array");
                obj.printArr(arr);
	}
}
