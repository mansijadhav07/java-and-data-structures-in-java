import java.util.*;
class Node{
	int data;
	Node next=null;
	Node(int data){
		this.data=data;
	}
}
class LinkedList{
	Node head=null;
	void addFirst(int data){
		Node newNode=new Node(data);
		if(head==null){
			head=newNode;
		}else{
			newNode.next=head;
			head=newNode;
		}
	}
	void addLast(int data){
		Node newNode=new Node(data);
		if(head==null){
			head=newNode;
		}else{
			Node temp=head;
			while(temp.next!=null){
				temp=temp.next;
			}
			temp.next=newNode;
		}
	}
	int countNode(){
		int count=0;
		Node temp=head;
		while(temp!=null){
			count++;
			temp=temp.next;
		}
		return count;
	}
	void addAtPos(int data,int pos){
		if(pos<=0 && pos==countNode()+2){
			System.out.println("wrong input");
			return;
		}
		if(pos==1){
			addFirst(data);
		}else if(pos==countNode()+1){
			addLast(data);
		}else{
			Node temp=head;
			Node newNode=new Node(data);
			while(pos-2!=0){

				temp=temp.next;
				pos--;
			}
			newNode.next=temp.next;
			temp.next=newNode;
		}
	}
	void delFirst(){
		if(head==null){
			System.out.println("Empty LL");
		}else if(countNode()==1){
			head=null;
		}else{
			head=head.next;
		}
	}
	void delLast(){
		 if(head==null){
                        System.out.println("Empty LL");
                }else if(countNode()==1){
                        head=null;
                }else{
                        Node temp=head;
			while(temp.next.next!=null){
				temp=temp.next;
			}
			
			temp.next=null;
                }
	}
	void delAtPos(int pos){
		if(pos<=0 && pos==countNode()+1){
                        System.out.println("wrong input");
                        return;
                }
                if(pos==1){
                        delFirst();
                }else if(pos==countNode()){
                        delLast();
                }else{
                        Node temp=head;
                        while(pos-2!=0){

                                temp=temp.next;
                                pos--;
                        }
                        temp.next=temp.next.next;
                }
	}
	void printLL(){
		if(head==null){
			System.out.println("Empty Linked List");
		}else{
			Node temp=head;
			while(temp.next!=null){
				System.out.print(temp.data+" ");
				temp=temp.next;
			}
			System.out.println(temp.data);
		}
	}
	void inplaceReverse(){
		Node current=head;
		Node previous=null;
		Node forward=null;
		while(current!=null){
			forward=current.next;
			current.next=previous;
			previous=current;
			current=forward;
		}
		head=previous;
	}
}
class Client{
	public static void main(String[] args){
		LinkedList ll=new LinkedList();
		char ch;
		do{
			System.out.println("1.addFirst\n2.addLast\n3.addAtPos\n4.delFirst\n5.delLAst\n6.delPos\n7.countNode\n8.Print Linked List\n9.In placereverse\n");
			Scanner sc=new Scanner(System.in);
			System.out.println("Enter choice");
			int choice=sc.nextInt();
			switch(choice){
				case 1:{
					       System.out.println("Enter data");
					       int data=sc.nextInt();
					       ll.addFirst(data);
					}
				       break;
			        case 2:{
                                               System.out.println("Enter data");
                                               int data=sc.nextInt();
                                               ll.addLast(data);
                                        }
                                       break;
				 case 3:{
                                               System.out.println("Enter data");
                                               int data=sc.nextInt();
					       System.out.println("Enter position");
					       int pos=sc.nextInt();
                                               ll.addAtPos(data,pos);
                                        }
                                       break;
				 case 4:ll.delFirst();
                                        break;
				 case 5:ll.delLast();
					break;
				case 6: {
                                               System.out.println("Enter position");
                                               int pos=sc.nextInt();
                                               ll.delAtPos(pos);
                                        }
                                       break;
				 case 7:{
						int count=ll.countNode();
						System.out.println("Count of nodes="+count);
				        }
					break;
				  case 8:ll.printLL();
					 break;
			          case 9:ll.inplaceReverse();
					 break;
				  default:
					 System.out.println("Invalid input");
					 break;
			}
			System.out.println("Do you want to continue?");
			ch=sc.next().charAt(0);
		}while(ch=='Y'||ch=='y');
	}
}







